这个是单独从muduo中提取出来的日志部分的代码，可以用Makefile单独编译，testLog.cpp为打印测试

**用法介绍如下**
Logger类定义了日志的打印打印方式，使用的时候只要包含这个头文件就行，日志打印定义了7个输出等级
默认是LOG_INFO,可以通过Logger::setLogLevel设置打印级别，方便调试，setOutput可设置日志的输出方式，默认打印到终端
2.timestamp类定义了时间的一些操作函数，通过now()获取Unix时间戳（1970年1月1日00:00到现在的秒数）
3.LogStream类主要重载了operator<<，用来输出各种格式数据，其中用到了一个缓冲区来保存数据
4.其中的main.cc是一个使用例子，打印之后格式如下：
日期      时间     级别 /*        输出正文（数据）     */ 源文件名：行号
20180730 10:41:18 TRACE updateChannel fd = 4 events = 3 - Poller.cc:57