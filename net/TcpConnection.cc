#include "TcpConnection.h"

#include "Channel.h"
#include "EventLoop.h"
#include "Socket.h"
#include "../base/logging.h"
#include "SocketsOps.h"
#include <unistd.h>
#include <functional>

void defaultConnectionCallback(const TcpConnectionPtr& conn)
{
	LOG_TRACE << conn->localAddress().toHostPort() << " -> "
		<< conn->peerAddress().toHostPort() << " is "
		<< (conn->connected() ? "UP" : "DOWN");
	// do not call conn->forceClose(), because some users want to register message callback only.
}

void defaultMessageCallback(const TcpConnectionPtr&,
	Buffer* buf,
	Timestamp)
{
	LOG_TRACE << "defaultMessageCallback: retrieveAll";
	buf->retrieveAll();
}

using namespace std::placeholders;		//bind
TcpConnection::TcpConnection(EventLoop* loop,
		const std::string& name,
		int sockfd,
		const InetAddress& localAddr,
		const InetAddress& peerAddr)
  : m_pLoop(CHECK_NOTNULL(loop)),
    m_strName(name),
	m_state(kConnecting),
	m_pSocket(new Socket(sockfd)),
	m_pChannel(new Channel(loop,sockfd)),
	m_localAddr(localAddr),
	m_peerAddr(peerAddr)
	{
		LOG_DEBUG << "TcpConnection::ctor[" << m_strName << "] at "
				  << this << " fd=" << sockfd;
		m_pChannel->setReadCallback(std::bind(&TcpConnection::handleRead, this,_1));
		m_pChannel->setWriteCallback(std::bind(&TcpConnection::handleWrite,this));
		m_pChannel->setErrorCallback(std::bind(&TcpConnection::handleError,this));
		m_pChannel->setCloseCallback(std::bind(&TcpConnection::handleClose,this));
	}

TcpConnection::~TcpConnection()
{
	LOG_DEBUG << "TcpConnection::dtor[" << m_strName << "] at "
				  << this << " fd=" << m_pChannel->fd();
	assert(m_state == kDisconnected);
}

void TcpConnection::connectEstablished()
{
	m_pLoop->assertInLoopThread();
	assert(m_state == kConnecting);
	setState(kConnected);
	m_pChannel->enableReading();

	m_connCallback(shared_from_this());		//打印连接信息
}

void TcpConnection::connectDestroyed()
{
	m_pLoop->assertInLoopThread();
	if (m_state == kConnected)
	{
		setState(kDisconnected);
		m_pChannel->disableAll();
	}
	m_connCallback(shared_from_this());		//打印连接信息
	m_pChannel->remove();
	
}

void TcpConnection::handleRead(Timestamp receiveTime)
{
	int savedErrno = 0;
	ssize_t n = m_inputBuffer.readFd(m_pChannel->fd(), &savedErrno);

	if (n > 0){
		m_msgCallback(shared_from_this(), &m_inputBuffer, receiveTime);
	}
	else if (0 == n){
		handleClose();
	}
	else{
		errno = savedErrno;
		LOG_SYSERR << "TcpConnection::handleRead";
		handleError();
	}
	
	// FIXME: close connection if n == 0
}

void TcpConnection::handleWrite()
{
	m_pLoop->assertInLoopThread();
	if (m_pChannel->isWriting()){
		ssize_t nwrote = ::write(m_pChannel->fd(),
								 m_outputBuffer.peek(), 
								 m_outputBuffer.readableBytes());
		if (nwrote > 0){
			m_outputBuffer.retrieve(nwrote);
			if (m_outputBuffer.readableBytes() == 0){
				m_pChannel->disableWriting();
				if (m_writeCompleteCallback)
				{
					m_pLoop->queueInLoop(std::bind(m_writeCompleteCallback, shared_from_this()));
				}
				//FIXME:那种情况到这？在非当前IO线程调用shutdown出现这个情况？
				if (m_state == kDisconnecting){
					shutdownInLoop();
				}
			}
			else{
				LOG_TRACE << "I'm going to write more data.";
			}
		}
		else{
			LOG_SYSERR << "TcpConnection::handleWrite";
		}
	}
	else{
		//FIXME:Connection is down?
		LOG_TRACE << "Connection is down, no more writing";
	}
}

void TcpConnection::handleClose()
{
	m_pLoop->assertInLoopThread();
	LOG_TRACE << "TcpConnection::handleClose state = " << m_state;
	assert(m_state == kConnected || m_state == kDisconnecting);
	//we don't close fd, leave it to dtor,so we can find leaks easily.
	setState(kDisconnected);
	m_pChannel->disableAll();

	//must be the last line
	m_closeCallback(shared_from_this());		//用于移除TcpServer中相应的TcpConnection
}

void TcpConnection::handleError()
{
	int err = sockets::getSocketError(m_pSocket->fd());
	LOG_ERROR << "TcpConnection::handleError [" << m_strName << "] - SO_ERROR = "
		<< err << " " << strerror_tl(err);
}

void TcpConnection::send(const void* data, size_t len)
{
	send(std::string(static_cast<const char*>(data), len));
}

void TcpConnection::send(const std::string& message)
{
	if (m_state == kConnected)
	{
		if (m_pLoop->isInLoopThread()){
			sendInLoop(message);
		}
		else{
			//使用move避免内存拷贝
			m_pLoop->runInLoop(std::bind(static_cast<void (TcpConnection::*) (const std::string&)>(&TcpConnection::sendInLoop),
				this, std::move(message)));
		}
	}
}

void TcpConnection::send(Buffer* buf)
{
	if (m_state == kConnected)
	{
		if (m_pLoop->isInLoopThread())
		{
			sendInLoop(buf->peek(), buf->readableBytes());
			buf->retrieveAll();
		}
		else
		{
			m_pLoop->runInLoop(std::bind(
				static_cast<void(TcpConnection::*)(const std::string&)>(&TcpConnection::sendInLoop),
				this, buf->retriveAllAsString()));
		}
	}
}

void TcpConnection::sendInLoop(const std::string& message)
{
	sendInLoop(message.c_str(), message.size());
}

void TcpConnection::sendInLoop(const void* data, size_t len)
{
	m_pLoop->assertInLoopThread();
	ssize_t nwrote = 0;
	size_t remaining = len;
	bool faultError = false;
	if (m_state == kDisconnected)
	{
		LOG_WARN << "disconnected, give up writing.";
		return;
	}
	//if no thing in m_outputBuffer,try writing directly.
	if (!m_pChannel->isWriting() && m_outputBuffer.readableBytes() == 0)
	{
		nwrote = ::write(m_pChannel->fd(), data, len);
		if (nwrote >= 0){
			remaining = len - nwrote;
			if (remaining == 0 && m_writeCompleteCallback)
			{
				m_pLoop->queueInLoop(std::bind(m_writeCompleteCallback, shared_from_this()));
			}
			if (static_cast<size_t>(nwrote) < len){
				LOG_TRACE << "I'm going to write more data.";
			}
		}
		else{		//nwrote < 0
			nwrote = 0;
			LOG_DEBUG << "TcpConnection::sendInLoop: ERRNO = " << errno << " : "
				<< strerror_tl(errno);
			//errno何时有效？
			if (errno != EWOULDBLOCK){
				LOG_SYSERR << "TcpConnection::sendInLoop";
				if (errno == EPIPE || errno == ECONNRESET)	//FIXME: any others?
				{
					faultError = true;
				}
			}
		}
	}

	//如果没有发送完成，插入m_outputBuffer，注册写事件
	assert(remaining <= len);
	if (!faultError && remaining > 0)
	{
		size_t oldLen = m_outputBuffer.readableBytes();
		if (oldLen + remaining >= m_highWaterMark
			&& oldLen < m_highWaterMark
			&& m_highWaterMarkCallback)
		{
			m_pLoop->queueInLoop(std::bind(m_highWaterMarkCallback, shared_from_this(), oldLen + remaining));
		}
		m_outputBuffer.append(static_cast<const char*>(data)+nwrote, remaining);
		if (!m_pChannel->isWriting()){
			m_pChannel->enableWriting();
		}
	}
}

void TcpConnection::setTcpNoDelay(bool on)
{
	m_pSocket->setTcpNoDelay(on);
}

void TcpConnection::shutdown()
{
	//FIXME: use compare and swap
	if (m_state == kConnected)
	{
		setState(kDisconnecting);
		//FIXME: shared_from_this?
		m_pLoop->runInLoop(std::bind(&TcpConnection::shutdownInLoop, this));
	}
}

void TcpConnection::shutdownInLoop()
{
	m_pLoop->assertInLoopThread();
	if (!m_pChannel->isWriting())
	{
		//We are not writing.
		m_pSocket->shutdownWrite();
	}
}

void TcpConnection::forceClose()
{
	// FIXME: use compare and swap
	if (m_state == kConnected || m_state == kDisconnecting)
	{
		setState(kDisconnecting);
		//queueInloop or runInLoop?
		m_pLoop->runInLoop(std::bind(&TcpConnection::forceCloseInLoop, shared_from_this()));
	}
}

void TcpConnection::forceCloseInLoop()
{
	m_pLoop->assertInLoopThread();
	if (m_state == kConnected || m_state == kDisconnecting)
	{
		// as if we received 0 byte in handleRead();
		handleClose();
	}
}

void TcpConnection::startRead()
{
	m_pLoop->runInLoop(std::bind(&TcpConnection::startReadInLoop, this));
}

void TcpConnection::startReadInLoop()
{
	m_pLoop->assertInLoopThread();
	if (!m_bReading || !m_pChannel->isReading())
	{
		LOG_DEBUG << "START READING";
		m_pChannel->enableReading();
		m_bReading = true;
	}
}

void TcpConnection::stopRead()
{
	m_pLoop->runInLoop(std::bind(&TcpConnection::stopReadInLoop, this));
}

void TcpConnection::stopReadInLoop()
{
	m_pLoop->assertInLoopThread();
	if (m_bReading || m_pChannel->isReading())
	{
		LOG_DEBUG << "STOP READING";
		m_pChannel->disableReading();
		m_bReading = false;
	}
}
