#ifndef NET_BUFFER_H
#define NET_BUFFER_H

#include <algorithm>
#include <string>
#include <vector>

#include <assert.h>

/// A buffer class modeled after org.jboss.netty.buffer.ChannelBuffer
///
/// @code
/// +-------------------+------------------+------------------+
/// | prependable bytes |  readable bytes  |  writable bytes  |
/// |                   |     (CONTENT)    |                  |
/// +-------------------+------------------+------------------+
/// |                   |                  |                  |
/// 0      <=      readerIndex   <=   writerIndex    <=     size
/// @endcode

class Buffer
{
public:
	static const size_t kCheapPrepend = 8;
	static const size_t kInitialSize = 1024;
	
	Buffer() :
		m_vecBuffer(kCheapPrepend + kInitialSize),
		m_readerIndex(kCheapPrepend),
		m_writerIndex(kCheapPrepend)
	{
		assert(readableBytes() == 0);
		assert(writeableBytes() == kInitialSize);
		assert(prependableBytes() == kCheapPrepend);
	}

	const size_t readableBytes()const
	{
		return m_writerIndex - m_readerIndex;
	}

	size_t writeableBytes()const
	{
		return m_vecBuffer.size() - m_writerIndex;
	}

	size_t prependableBytes()const
	{
		return m_readerIndex;
	}

	const char* peek()const
	{
		return begin() + m_readerIndex;
	}

	char* beginWrite()
	{
		return begin() + m_writerIndex;
	}

	const char* beginWrite() const
	{
		return begin() + m_writerIndex;
	}

	const char* findCRLF()const
	{
		const char* crlf = std::search(peek(), beginWrite(), kCRLF, kCRLF + 2);
		return crlf == beginWrite() ? NULL : crlf;
	}

	const char* findCRLF(const char* start) const
	{
		assert(peek() <= start);
		assert(start <= beginWrite());
		// FIXME: replace with memmem()?
		const char* crlf = std::search(start, beginWrite(), kCRLF, kCRLF + 2);
		return crlf == beginWrite() ? NULL : crlf;
	}

	void hasWritten(size_t len)
	{
		m_writerIndex += len;
	}

	// retrieve returns void, to prevent
	// string str(retrieve(readableBytes()), readableBytes());
	// the evaluation of two functions are unspecified
	void retrieve(size_t len)
	{
		assert(len <= readableBytes());
		m_readerIndex += len;
	}
	//�޸�m_readerIndex��end
	void retrieveUntil(const char* end)
	{
		assert(peek() <= end);
		assert(end <= beginWrite());
		retrieve(end - peek());
	}

	void retrieveAll()
	{
		m_readerIndex = kCheapPrepend;
		m_writerIndex = kCheapPrepend;
	}

	std::string retrieveAsString(size_t len)
	{
		assert(len <= readableBytes());
		std::string result(peek(), len);
		retrieve(len);
		return result;
	}

	void retrive(size_t len)
	{
		assert(len <= readableBytes());
		if (len < readableBytes())
		{
			m_readerIndex += len;
		}
		else
		{
			retrieveAll();
		}
	}

	std::string retriveAllAsString()
	{
		return retrieveAsString(readableBytes());
	}

	void append(const std::string& str)
	{
		append(str.data(), str.size());
	}

	void append(const char* /*restrict*/ data, size_t len)
	{
		ensureWritableBytes(len);
		std::copy(data, data + len, beginWrite());
		hasWritten(len);
	}

	void append(const void* /*restrict*/ data, size_t len)
	{
		append(static_cast<const char*>(data), len);
	}

	void ensureWritableBytes(size_t len)
	{
		if (writeableBytes() < len)
		{
			makeSpace(len);
		}
		assert(writeableBytes() >= len);
	}
	
	void prepend(const void* /*restrict*/ data, size_t len)
	{
		assert(len <= prependableBytes());
		m_readerIndex -= len;
		const char* d = static_cast<const char*>(data);
		std::copy(d, d + len, begin() + m_readerIndex);
	}

	void shrink(size_t reserve)
	{
		// move readable data to the buf front, then swap with m_vecBuffer
		std::vector<char> buf(kCheapPrepend + readableBytes() + reserve);
		std::copy(peek(), peek() + readableBytes(), buf.begin() + kCheapPrepend);
		buf.swap(m_vecBuffer);
	}

	/// Read data directly into buffer.
	///
	/// It may implement with readv(2)
	/// @return result of read(2), @c errno is saved
	ssize_t readFd(int fd, int* savedErrno);
private:
	char* begin()
	{
		return &*m_vecBuffer.begin();
	}

	const char* begin()const
	{
		return &*m_vecBuffer.begin();
	}

	void makeSpace(size_t len)
	{
		if (prependableBytes() + writeableBytes() < len + kCheapPrepend){
			m_vecBuffer.resize(m_writerIndex + len);
		}
		else
		{
			// move readable data to the front, make space inside buffer
			assert(kCheapPrepend < m_readerIndex);
			size_t readable = readableBytes();
			std::copy(begin() + m_readerIndex,
				begin() + m_writerIndex,
				begin() + kCheapPrepend);
			m_readerIndex = kCheapPrepend;
			m_writerIndex = m_readerIndex + readable;
			assert(readable == readableBytes());
		}
	}

	std::vector<char> m_vecBuffer;
	size_t m_readerIndex;
	size_t m_writerIndex;

	static const char kCRLF[];
};
#endif //NET_BUFFER_H