#include "EventLoop.h"
#include "TimerQueue.h"

#include "../base/logging.h"
#include "Poller.h"
#include "Channel.h"

#include <assert.h>
#include <poll.h>
#include <sstream>
#include <string>

#include <sys/eventfd.h>
#include <unistd.h>
#include <signal.h>

using namespace std;

__thread EventLoop* t_loopInThisThread = 0;
const int kPollTimeMs = 1000;

static int createEventfd()
{
	int evtfd = ::eventfd(0, EFD_NONBLOCK | EFD_CLOEXEC);
	if (evtfd < 0)
	{
		LOG_SYSERR << "Failed in eventfd";
		abort();
	}
	return evtfd;
}

#pragma GCC diagnostic ignored "-Wold-style-cast"
class IgnoreSigPipe
{
public:
	IgnoreSigPipe()
	{
		::signal(SIGPIPE, SIG_IGN);
		// LOG_TRACE << "Ignore SIGPIPE";
	}
};
#pragma GCC diagnostic error "-Wold-style-cast"

IgnoreSigPipe initObj;

EventLoop::EventLoop()
  : looping_(false),
	quit_(false),
	callingPendingFunctors_(false),
    threadId_(this_thread::get_id()),
	poller_(new Poller(this)),
	timerQueue_(new TimerQueue(this)),
	wakeupFd_(createEventfd()),
	wakeupChannel_(new Channel(this, wakeupFd_))
{
	std::stringstream ss;
	ss << threadId_;
	LOG_INFO << "EventLoop created " << this << " in thread " << ss.str();

	if (t_loopInThisThread)
	{
		LOG_FATAL << "Another EventLoop " << t_loopInThisThread
			<< " exists in this thread " << ss.str();
	}
	else
	{
		t_loopInThisThread = this;
	}
	wakeupChannel_->setReadCallback(
		std::bind(&EventLoop::handleRead,this));
	// we are always reading the wakeupfd
	wakeupChannel_->enableReading();
}

EventLoop::~EventLoop()
{
	LOG_DEBUG << "~EventLoop()::dtor";
	assert(!looping_);
	wakeupChannel_->disableAll();
	wakeupChannel_->remove();
	::close(wakeupFd_);
	t_loopInThisThread = NULL;
}

EventLoop* EventLoop::getEventLoopOfCurrentThread()
{
	return t_loopInThisThread;
}

void EventLoop::loop()
{
	assert(!looping_);
	assertInLoopThread();
	looping_ = true;
	quit_ = false;
	
	while(!quit_)
	{
		activeChannels_.clear();
		pollReturnTime_ = poller_->poll(kPollTimeMs, &activeChannels_);
		for(ChannelList::iterator it = activeChannels_.begin(); it != activeChannels_.end(); it++)
		{
			(*it)->handleEvent(pollReturnTime_);
		}
		doPendingFunctors();
	}
	LOG_TRACE << "EventLoop " << this << " stop looping";
	looping_ = false;
}

void EventLoop::abortNotInLoopThread()
{
	std::stringstream ss1;
	ss1 << threadId_;
	std::stringstream ss2;
	ss2 << this_thread::get_id();
	LOG_FATAL << "EventLoop::abortNotInLoopThread - EventLoop " << this
		<< " was created in threadId_ = " << ss1.str() << ", current thread id = "
		<< ss2.str() << " exit!";
}

void EventLoop::quit()
{
	quit_ = true;
	//wakeup();
	//如果非当前线程需要唤醒
	if(!isInLoopThread())
	{
		wakeup();
	}
	
}

void EventLoop::updateChannel(Channel* channel)
{
  assert(channel->ownerLoop() == this);
  assertInLoopThread();
  poller_->updateChannel(channel);
}

TimerId EventLoop::runAt(const Timestamp& time, const TimerCallback& cb)
{
	return timerQueue_->addTimer(cb, time, 0.0);
}

TimerId EventLoop::runAfter(double delay, const TimerCallback& cb)
{
	Timestamp time(addTime(Timestamp::now(), delay));
	return runAt(time, cb);
}

TimerId EventLoop::runEvery(double interval, const TimerCallback& cb)
{
	Timestamp time(addTime(Timestamp::now(), interval));
	return timerQueue_->addTimer(cb, time, interval);
}

TimerId EventLoop::runAt(const Timestamp& time, TimerCallback&& cb)
{
	return timerQueue_->addTimer(std::move(cb), time, 0.0);
}

TimerId EventLoop::runAfter(double delay, TimerCallback&& cb)
{
	Timestamp time(addTime(Timestamp::now(), delay));
	return runAt(time, std::move(cb));
}

TimerId EventLoop::runEvery(double interval, TimerCallback&& cb)
{
	Timestamp time(addTime(Timestamp::now(), interval));
	return timerQueue_->addTimer(std::move(cb), time, interval);
}

void EventLoop::cancel(TimerId timerId)
{
	timerQueue_->cancel(timerId);
}

void EventLoop::doPendingFunctors()
{
	std::vector<Functor> functors;
	callingPendingFunctors_ = true;
	
	{
		std::lock_guard<std::mutex> lck(mutex_);
		functors.swap(pendingFunctors_);
	}
	
	for(auto func : functors)
	{
		func();
	}
	
	callingPendingFunctors_ = false;
}

void EventLoop::runInLoop(const Functor& cb)
{
	if(isInLoopThread()){
		cb();
	}else{
		queueInLoop(cb);
	}
}

void EventLoop::queueInLoop(const Functor& cb)
{
	{
		std::lock_guard<std::mutex> lc(mutex_);
		pendingFunctors_.push_back(cb);
	}
	
	//这里唤醒poll的情况有两种，其他线程调用queueloop，那么唤醒是必须的
	//还有如果是当前IO线程正在调用pending functor，那么也必须唤醒，
	//也是为了pending factor执行完毕以后及时执行新加入的factor
	if(!isInLoopThread() || callingPendingFunctors_)
	{
		if (looping_)  wakeup();
	}
}

void EventLoop::wakeup()
{
	uint64_t one = 1;
	ssize_t n = ::write(wakeupFd_,&one,sizeof(one));
	if (n != sizeof one)
	{
		LOG_ERROR << "EventLoop::handleRead() reads " << n << " bytes instead of 8";
	}
}

void EventLoop::handleRead()
{
	uint64_t one = 1;
	ssize_t n = ::read(wakeupFd_, &one, sizeof(one));
	if( n!=sizeof(uint64_t))
	{
		LOG_ERROR << "EventLoop::handleRead() reads " << n << " bytes instead of 8";
	}
}

//只在当前Loop中使用这个函数
void EventLoop::removeChannel(Channel* channel)
{
	assert(channel->ownerLoop() == this);
	assertInLoopThread();
	poller_->removeChannel(channel);
}