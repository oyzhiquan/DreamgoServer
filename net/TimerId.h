#ifndef NET_TIMERID_H
#define NET_TIMERID_H
class Timer;

///
/// An opaque identifier, for canceling Timer.
///
class TimerId
{
public:
	explicit TimerId(Timer* timer,int64_t seq)
	: value_(timer),
	  sequence_(seq)
	{
	}
	TimerId()
		: value_(NULL),
		sequence_(0)
	{
	}
	// default copy-ctor, dtor and assignment are okay
	friend class TimerQueue;
private:
	Timer* value_;
	int64_t sequence_;
};


#endif  //NET_TIMERID_H
