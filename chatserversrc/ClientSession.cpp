#include "ClientSession.h"
#include "../base/logging.h"
#include "../net/protocolstream.h"
#include "../net/TcpConnection.h"
#include "Msg.h"
#include <cstring>

ClientSession::ClientSession(const std::shared_ptr<TcpConnection>& tmpConn) :
TcpSession(tmpConn),
m_id(0),
m_seq(0)
{
	m_userinfo.userid = 0;
}

ClientSession::~ClientSession()
{

}

//+------------+----------------------------------------------------------------------+
//|    包头    |								数据								  |
//+------------+-------------+-------------+-------------+----------+----------+------+
//|    msg     |   PACKLEN   |CHECKSUM_LEN | buf(headlen)| cmd(int) | seq(int) | data |
//+------------+-------------+-------------+-------------+----------+----------+------+
void ClientSession::OnRead(const TcpConnectionPtr& conn, Buffer* pBuffer, Timestamp receiveTime)
{
	while (true)
	{
		//不够一个包头大小
		if (pBuffer->readableBytes() < (size_t)sizeof(msg))
		{
			LOG_INFO << "buffer is not enough for a package header, pBuffer->readableBytes()=" << pBuffer->readableBytes() << ", sizeof(msg)=" << sizeof(msg);
			return;
		}
		//不够一个整包大小
		msg header;
		memcpy(&header, pBuffer->peek(), sizeof(msg));
		if (pBuffer->readableBytes() < (size_t)sizeof(msg)+(size_t)header.packagesize)
		{
			LOG_INFO << "buffer is not enough for a package";
			return;
		}

		pBuffer->retrieve(sizeof(msg));
		std::string inbuf;
		inbuf.append(pBuffer->peek(), header.packagesize);
		pBuffer->retrieve(header.packagesize);
		if (!Process(conn, inbuf.c_str(), inbuf.length()));
		{
			LOG_WARN << "Process error,close TcpConnection";
			conn->forceClose();
		}
	}// end while-loop
}

//收到一个message以后，inbuf指向去掉msg头以后的message,length是数据长度
bool ClientSession::Process(const std::shared_ptr<TcpConnection>& conn, const char* inbuf, size_t length)
{
	yt::BinaryReadStream2 readStream(inbuf, length);
	int cmd;
	if (!readStream.Read(cmd))
	{
		LOG_ERROR << "READ CMD ERROR!!!";
		return false;
	}
	if (!readStream.Read(m_seq))
	{
		LOG_WARN << "READ SEQ ERROR!!!";
		return false;
	}
	std::string data;
	size_t dataLength;
	if (!readStream.Read(&data, 0, dataLength))
	{
		LOG_WARN << "READ DATA ERROR!!!";
		return false;
	}

	LOG_INFO << "Recv from client: cmd=" << cmd << ", seq=" << m_seq << ", header.packagesize:" << length << ", data=" << data << ", datalength=" << dataLength;
	LOG_DEBUG_BIN((unsigned char*)inbuf, length);

	switch (cmd)
	{
		//心跳包
		case msg_type_heartbeart:
		{
			OnHeartbeatResponse(conn);
		}
			break;
		default:
			LOG_WARN << "unsupport cmd, cmd:" << cmd << ", data=" << data << ", connection name:" << conn->peerAddress().toHostPort();
			return false;
	}//end switch
	++m_seq;
	return true;
}

void ClientSession::OnHeartbeatResponse(const TcpConnectionPtr conn)
{
	std::string outbuf;
	yt::BinaryWriteStream3 writeStream(&outbuf);
	writeStream.Write(msg_type_heartbeart);
	writeStream.Write(m_seq);
	std::string dummy;
	writeStream.Write(dummy.c_str(), dummy.length());
	writeStream.Flush();
	LOG_INFO << "Response to client: cmd=1000" << ", sessionId=" << m_id;
	Send(outbuf);
}