/**
* ClientSession.h
**/

#pragma once
#include "../net/Buffer.h"
#include "TcpSession.h"

struct OnlineUserInfo
{
	int32_t     userid;
	std::string username;
	std::string nickname;
	std::string password;
	int32_t     clienttype;     //客户端类型,pc=1, android=2, ios=3
	int32_t     status;         //在线状态 0离线 1在线 2忙碌 3离开 4隐身
};

class ClientSession : public TcpSession
{
public:
	ClientSession(const std::shared_ptr<TcpConnection>& tmpConn);
	virtual ~ClientSession();

	ClientSession(const ClientSession& rhs) = delete;
	ClientSession& operator =(const ClientSession& rhs) = delete;

	void OnRead(const TcpConnectionPtr& conn, Buffer* pBuffer, Timestamp receiveTime);

	int32_t GetUserId()
	{
		return m_userinfo.userid;
	}

	void SendUserStatusChangeMsg(int32_t userid, int type);
private:
	bool Process(const std::shared_ptr<TcpConnection>& conn, const char* inbuf, size_t length);

	void OnHeartbeatResponse(const TcpConnectionPtr conn);
private:
	int32_t					m_id;			//session id
	OnlineUserInfo			m_userinfo;
	int						m_seq;			//当前Session数据包序列号
};