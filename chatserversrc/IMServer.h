/**
*  服务器主服务类，IMServer.h
*  2018.8.14
**/

#pragma once
#include <memory>
#include <map>
#include <mutex>
#include "../net/TcpServer.h"
#include "../net/EventLoop.h"
#include "ClientSession.h"

typedef std::shared_ptr<ClientSession> ClientSessionPtr;
struct StoredUserInfo
{
	int32_t         userid;
	std::string     username;
	std::string     password;
	std::string     nickname;
};

class IMServer final
{
public:
	IMServer() = default;
	~IMServer() = default;

	IMServer(const IMServer& rhs) = delete;
	IMServer& operator =(const IMServer& rhs) = delete;

	bool Init(const char* ip, short port, EventLoop* loop);
private:
	//新连接到来或连接断开时候调用，所以需要通过conn->connected()来判断，一般只在主loop（工作loop?）里面调用
	void OnConnection(std::shared_ptr<TcpConnection> conn);
	//连接断开
	void OnClose(const std::shared_ptr<TcpConnection>& conn);

private:
	std::shared_ptr<TcpServer>					m_server;
	std::map<std::string,ClientSessionPtr>		m_mapSessions;
	std::mutex									m_sessionMutex;		//多线程之间保护m_sessions
	int											m_baseUserId{};		//初始化为0	C++11
	std::mutex									m_idMutex;			////多线程之间保护m_baseUserId
};