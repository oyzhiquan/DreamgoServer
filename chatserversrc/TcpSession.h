/**
* TcpSession.h
* 2018.08.14
**/

#pragma once

#include <memory>
#include "../net/TcpConnection.h"
//为了让业务与逻辑分开，实际应该新增一个子类继承自TcpSession，让TcpSession中只有逻辑代码，其子类存放业务代码
class TcpSession
{
public:
	TcpSession(const std::weak_ptr<TcpConnection>& tmpconn);
	~TcpSession();

	TcpSession(const TcpSession& rhs) = delete;
	TcpSession& operator=(const TcpSession& rhs) = delete;

	TcpConnectionPtr GetConnectionPtr(){ return m_tmpConn.lock(); }

	void Send(const std::string& buf);
	void Send(const char* p, int length);
protected:
	std::weak_ptr<TcpConnection>    m_tmpConn;
};