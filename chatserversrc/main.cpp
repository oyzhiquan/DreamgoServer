#include "../base/logging.h"
#include "../jsoncpp-0.5.0/json.h"

#include <stdio.h>
#include <memory>
#include <iostream>

struct User
{
	string         username;    //群账户的username也是群号userid的字符串形式
	string         password;
	string         nickname;    //群账号为群名称
};
using namespace std;

int main(int argc, char* argv[])
{
	string data = "{\"username\": \"13576603915\", \"nickname\": \"kitty\", \"password\": \"123456\"}";
	cout << data << endl;
	Json::Reader JsonReader;
	Json::Value JsonRoot;
	if (!JsonReader.parse(data, JsonRoot))
	{
		LOG_WARN << "invalid json1: ";
		return 0;
	}

	if (!JsonRoot["username"].isString() || !JsonRoot["nickname"].isString() || !JsonRoot["password"].isString())
	{
		LOG_WARN << "invalid json2: ";
		return 0;
	}

	User u;
	u.username = JsonRoot["username"].asString();
	u.nickname = JsonRoot["nickname"].asString();
	u.password = JsonRoot["password"].asString();
	return 0;
}