#include "../net/InetAddress.h"
#include "../base/logging.h"
#include "../base/singleton.h"
#include "IMServer.h"
#include "ClientSession.h"
using namespace placeholders;

//Server��ʼ������������
bool IMServer::Init(const char* ip, short port, EventLoop* loop)
{
	InetAddress addr(ip, port);
	m_server = std::make_shared<TcpServer>(loop, addr, "ZRJ-MYIMSERVER", TcpServer::kNoReusePort);
	m_server->setConnCallback(std::bind(&IMServer::OnConnection, this, _1));
	//��������
	m_server->start();
	return true;
}

void IMServer::OnConnection(TcpConnectionPtr conn)
{
	if (conn->connected())
	{
		LOG_INFO << "client connected:" << conn->peerAddress().toHostPort();
		++m_baseUserId;
		ClientSessionPtr spSession = std::make_shared<ClientSession>(conn);
		conn->setMsgCallback(std::bind(&ClientSession::OnRead, spSession.get(), _1,_2,_3));
		std::string nameConn = conn->name();

		{
			std::lock_guard<std::mutex> lock(m_sessionMutex);
			m_mapSessions[nameConn] = spSession;
		}
	}
	else
	{
		OnClose(conn);
	}
}

void IMServer::OnClose(const TcpConnectionPtr& conn)
{
	std::string nameConn = conn->name();
	{
		std::lock_guard<std::mutex> guard(m_sessionMutex);
		int count = m_mapSessions.erase(nameConn);
		assert(count == 1);
	}
	LOG_INFO << "client disconnected: " << conn->peerAddress().toHostPort();
	LOG_INFO << "current online user count: " << m_mapSessions.size();
}